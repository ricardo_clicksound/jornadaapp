import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";

export default function Loading() {
  return (
    <View style={styles.loadingContainer}>
      <Image
        source={require("../../assets/img/jornada-logo.png")}
        style={styles.logo}
        resizeMode="contain"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  loadingContainer: {
    backgroundColor: "#763a9c",
    height: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
