import React, { useState } from "react";
import { StyleSheet, View, AsyncStorage } from "react-native";
import { Icon, Input, Button } from "react-native-elements";
import {
  withNavigation,
  NavigationActions,
  StackActions
} from "react-navigation";

function codeValidateForm(props) {
  const { toastRef, navigation } = props;
  const [code, setCode] = useState("");
  const validateCode = async () => {
    if (!code) {
      toastRef.current.show("Es obligatorio introducir el código.");
    } else {
      //Validar formato del código y luego hacer la consulta
      if (code === "TEST") {
        try {
          await AsyncStorage.setItem("code_jornada", code);
          await AsyncStorage.setItem("loged", "true");
          toastRef.current.show("Código validado correctamente..");
          const resetAction = StackActions.reset({
            index: 0,
            key: null,
            actions: [NavigationActions.navigate({ routeName: "Jornada" })]
          });
          navigation.dispatch(resetAction);
        } catch (error) {
          toastRef.current.show("Ha ocurrido un error.");
          console.log(error);
        }
      }
    }
  };

  return (
    <View>
      <Input
        placeholder="Introduce el código"
        onChange={e => setCode(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name="code-not-equal-variant"
            iconStyle={styles.iconRight}
          />
        }
      />
      <Button
        title="Validar"
        containerStyle={styles.btnContainerValidate}
        onPress={validateCode}
      />
    </View>
  );
}

export default withNavigation(codeValidateForm);

const styles = StyleSheet.create({
  iconRight: {},
  btnContainerValidate: {
    marginTop: 20,
    width: "95%"
  }
});
