import * as React from "react";
import { StackActions, NavigationActions } from "react-navigation";
import { DrawerActions } from "react-navigation-drawer";

export const navigationRef = React.createRef();

export function navigate(name, params) {
  try {
    navigationRef.current &&
      navigationRef.current.dispatch(
        StackActions.reset({
          index: 0,
          key: null,
          actions: [NavigationActions.navigate({ routeName: "Jornada" })]
        })
        //   DrawerActions.closeDrawer()
      );
    navigationRef.current.dispatch(DrawerActions.closeDrawer());
    navigationRef.closeDrawer;
  } catch (error) {
    console.log(error);
  }
}
