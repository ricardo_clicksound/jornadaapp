import { AsyncStorage } from "react-native";

export const isLogeado = async () => {
  try {
    // await AsyncStorage.setItem("loged", "null"); //Invalidar logeo
    return await AsyncStorage.getItem("loged");
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const cerrarSesion = async () => {
  try {
    AsyncStorage.setItem("loged", "false");
    return false;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const setSesion = () => {};
