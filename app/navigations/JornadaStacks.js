import React, { useState } from "react";
import { createStackNavigator } from "react-navigation-stack";
import JornadaScreen from "../screens/Jornada/Jornada";
import FormCodigoScreen from "../screens/Jornada/FormCodigo";
import EscanearCodigoScreen from "../screens/Jornada/QRScanner";
import PanelTrabajadorScreen from "../screens/Jornada/PanelTrabajador";
import { Icon } from "react-native-elements";

function efisima() {
  const [mostrarHeader, setMostrarHeader] = useState();
}

const JornadaScreenStacks = createStackNavigator(
  {
    Jornada: {
      screen: JornadaScreen,
      navigationOptions: () => ({
        title: "Valida tu código",
        headerRight: () => <Icon type="material-community" name="phone" />,
        headerTintColor: "white",
        headerStyle: {
          backgroundColor: "#753e9c"
        }
      })
    },
    FormCodigo: {
      screen: FormCodigoScreen,
      navigationOptions: () => ({
        title: "Introduce tu código",
        headerRight: () => <Icon type="material-community" name="phone" />,
        headerTintColor: "white",
        headerStyle: {
          backgroundColor: "#753e9c"
        }
      })
    },
    EscanearCodigo: {
      screen: EscanearCodigoScreen,
      navigationOptions: () => ({
        title: "Escanea el código QR",
        headerRight: () => <Icon type="material-community" name="phone" />,
        headerTintColor: "white",
        headerStyle: {
          backgroundColor: "#753e9c"
        }
      })
    },
    PanelTrabajador: {
      screen: PanelTrabajadorScreen,
      navigationOptions: () => ({
        title: "Mi Jornada",
        headerLeft: () => null,
        headerRight: () => <Icon type="material-community" name="phone" />,
        headerTintColor: "white",
        headerStyle: {
          backgroundColor: "#753e9c"
        }
      })
    }
  },
  {
    order: ["Jornada", "FormCodigo", "EscanearCodigo", "PanelTrabajador"]
  }
);

export default JornadaScreenStacks;
