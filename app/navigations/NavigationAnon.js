import React from "react";
import { createDrawerNavigator } from "react-navigation-drawer";
import JornadaScreenStacks from "./JornadaStacks";
import CustomComponent from "./CustomDrawerComponent";

const NavigationStacksAnon = createDrawerNavigator(
  {
    Jornada: {
      screen: JornadaScreenStacks,
      navigationOptions: () => ({
        topBarLabel: "Jornada"
      })
    }
  },
  {
    contentComponent: props => <CustomComponent {...props} />,
    order: ["Jornada"]
  }
);

export default NavigationStacksAnon;
