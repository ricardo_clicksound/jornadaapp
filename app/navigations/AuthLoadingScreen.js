import React, { useEffect, useState } from "react";
import Loading from "../components/Loading";
import { withNavigation } from "react-navigation";
import { isLogeado } from "../utils/acces_control";
import { AsyncStorage } from "react-native";

function AuthLoadingScreen(props) {
  const { navigation } = props;
  const [isLogged, setIsLogged] = useState(null);
  useEffect(() => {
    async function logeado() {
      const userState = await AsyncStorage.getItem("logged");
      navigation.navigate(userState === "true" ? "Jornada" : "JornadaAnon");
    }
    logeado();
    console.log("me acabo de ejecutar");
  }, [isLogged]);
  return <Loading />;
}

export default withNavigation(AuthLoadingScreen);
