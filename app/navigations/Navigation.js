import React from "react";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import NavigationStacksLogged from "./NavigationLogged";
import NavigationStacksAnon from "./NavigationAnon";
import AuthLoadingScreen from "./AuthLoadingScreen";
import { createStackNavigator } from "react-navigation-stack";

const JornadaStack = createStackNavigator({ NavigationStacksLogged });
const JornadaAnonStack = createStackNavigator({ NavigationStacksAnon });

const NavigationStacks = createSwitchNavigator(
  {
    Starter: AuthLoadingScreen,
    Jornada: JornadaStack,
    JornadaAnon: JornadaAnonStack
  },
  {
    order: ["Jornada", "JornadaAnon"]
  }
);

export default createAppContainer(NavigationStacks);
