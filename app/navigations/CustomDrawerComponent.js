import React, { useState, useEffect } from "react";
import { View, ScrollView, StyleSheet } from "react-native";
import SafeAreaView from "react-native-safe-area-view";
import { DrawerNavigatorItems } from "react-navigation-drawer";
import { Icon } from "react-native-elements";
import { cerrarSesion, isLogeado } from "../utils/acces_control";
import {
  withNavigation,
  NavigationActions,
  StackActions
} from "react-navigation";

function CustomDrawerContentComponent(props) {
  const [es_logeado, setEs_logeado] = useState(false);
  const { navigation } = props;
  useEffect(() => {
    async function logeado() {
      let login = await isLogeado();
      console.log("entro");
      if (login === "true") {
        setEs_logeado(true);
      }
    }
    logeado();
  }, [es_logeado]);
  return (
    <ScrollView>
      {es_logeado && (
        <View style={styles.logoutButton}>
          <Icon
            type="material-community"
            name="power-standby"
            onPress={() => {
              cerrarSesion();
              const resetAction = StackActions.reset({
                index: 0,
                key: null,
                actions: [NavigationActions.navigate({ routeName: "Jornada" })]
              });
              navigation.dispatch(resetAction);
            }}
          />
        </View>
      )}
      <SafeAreaView>
        <DrawerNavigatorItems {...props} />
      </SafeAreaView>
    </ScrollView>
  );
}

export default withNavigation(CustomDrawerContentComponent);

const styles = StyleSheet.create({
  logoutButton: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    marginTop: 40
  }
});
