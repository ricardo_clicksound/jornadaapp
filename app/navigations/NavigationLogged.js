import React from "react";
import { createDrawerNavigator } from "react-navigation-drawer";
import JornadaScreenStacks from "./JornadaStacks";

const NavigationStacksLogged = createDrawerNavigator(
  {
    Jornada: {
      screen: JornadaScreenStacks,
      navigationOptions: () => ({
        topBarLabel: "Jornada"
      })
    }
  },
  {
    order: ["Jornada"]
  }
);

export default NavigationStacksLogged;
