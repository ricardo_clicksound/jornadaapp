import React, { useState, useEffect, useRef } from "react";
import { View, Text, AsyncStorage } from "react-native";
import ScanScreen from "../../components/Jornada/ScanScreen";
import { withNavigation } from "react-navigation";
import Toast from "react-native-easy-toast";
import { NavigationActions, StackActions } from "react-navigation";
import PanelTrabajador from "./PanelTrabajador";

function QRScanner(props) {
  const toastRef = useRef();
  const { navigation } = props;
  const [code, setCode] = useState(null);

  useEffect(() => {
    if (code == "TEST") {
      const _storeData = async () => {
        try {
          await AsyncStorage.setItem("code_jornada", code);
          await AsyncStorage.setItem("loged", "true");
          const resetAction = StackActions.reset({
            index: 0,
            key: null,
            actions: [NavigationActions.navigate({ routeName: "Jornada" })]
          });
          navigation.dispatch(resetAction);
        } catch (error) {
          console.log(error);
        }
      };
      _storeData();
    }
  }, [code]);

  return (
    <>
      <View>
        <Text>Escanea el código QR:</Text>
        <Toast ref={toastRef} position="center" opacity={0.5} />
      </View>
      <ScanScreen toastRef={toastRef} setCode={setCode} code={code} />
    </>
  );
}

export default withNavigation(QRScanner);
