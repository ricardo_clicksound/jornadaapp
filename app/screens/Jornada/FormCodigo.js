import React, { useRef } from "react";
import { View } from "react-native";
import CodeValidateForm from "../../components/Jornada/CodeValidateForm";
import Toast from "react-native-easy-toast";

export default function FormCodigo() {
  const toastRef = useRef();
  return (
    <View>
      <CodeValidateForm toastRef={toastRef} />
      <Toast ref={toastRef} position="center" opacity={0.8} />
    </View>
  );
}
