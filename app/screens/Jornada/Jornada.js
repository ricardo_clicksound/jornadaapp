import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import { Button, Divider } from "react-native-elements";
import { withNavigation } from "react-navigation";
import { isLogeado } from "../../utils/acces_control";
import { StackActions, NavigationActions } from "react-navigation";
import Loading from "../../components/Loading";

function Jornada(props) {
  const { navigation } = props;
  const [loged, setLoged] = useState(null);
  useEffect(() => {
    navigation.closeDrawer();
    async function logeado() {
      let login = await isLogeado();
      if (login === "true") {
        // setLoged(true); //Si se activa esto se vuelve a actualizar el componente ya que ha cambiado el estado de loged!
        const resetAction = StackActions.reset({
          index: 0,
          key: null,
          actions: [NavigationActions.navigate(Jornada)]
        });
        navigation.dispatch(resetAction);
      } else {
        setLoged(false);
      }
    }
    logeado();
  }, [loged]);

  if (loged === false) {
    return (
      <View>
        <Button
          title="Escanear QR"
          onPress={() => navigation.navigate("EscanearCodigo")}
        />
        <Divider />
        <Button
          title="Introduce un código"
          onPress={() => navigation.navigate("FormCodigo")}
        />
      </View>
    );
  }
  return <Loading />;
}

export default withNavigation(Jornada);
