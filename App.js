import React from "react";
import Navigation from "./app/navigations/Navigation";
import { navigationRef } from "./app/utils/RootNavigation";

export default function App() {
  return <Navigation ref={navigationRef} />;
}
